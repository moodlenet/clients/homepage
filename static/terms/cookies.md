# **MoodleNet Cookies Policy**

*Effective: 1st of November 2019*

The operators of this MoodleNet site use cookies on this website and all affiliated websites (collectively the "Site").

Our Cookies Policy explains what cookies are, how we use cookies, how third-parties we partner with may use cookies on the Site, and your choices regarding cookies. Please read this Cookies Policy in conjunction with our Privacy Notice (see below), which sets out additional details on how we use personally identifiable information and your various rights.

## **What are cookies**

Cookies are small pieces of text sent by your web browser by a website you visit. A cookie file is stored in your web browser and allows the Site or a third-party to recognize you and make your next visit easier and the Site more useful to you. Essentially, cookies are a user’s identification card for our servers. Cookies allow us to serve you better and more efficiently, and to personalize your experience on our Site.

Cookies can be "persistent" or "session" cookies.

## **How MoodleNet uses cookies**

When you use and access the Site, we may place a number of cookies files in your web browser.

MoodleNet uses or may use cookies to help us determine and identify repeat visitors, the type of content and sites to which a user of our Site links, the length of time each user spends at any particular area of our Site, and the specific functionalities that users choose to use. To the extent that cookies data constitutes personally identifiable information, we process such data on the basis of your consent.

We use both session and persistent cookies on the Site and we use different types of cookies to run the Site:

-   _Essential cookies._ Necessary for the operation of the Site. We may use essential cookies to authenticate users, prevent fraudulent use of user accounts, or offer Site features.
-   _Analytical/performance cookies._ Allow us to recognize and count the number of visitors and see how visitors move around the Site when using it. This helps us improve the way the Site works.
-   _Functionality cookies._ Used to recognise you when you return to the Site. This enables us to personalise our content for you, greet you by name, and remember your preferences (for example, your choice of language or region).

To view a list of MoodleNet cookies, please view ourCookies table below.

## **Third-party cookies**

In addition to our own cookies, we may also use various third-party cookies to report usage statistics of the Site.

-   _Tracking cookies._ Follow on-site behaviour and tie it to other metrics allowing better understanding of usage habits.

To view a list of third-party cookies that we use, please view our Cookies table below.

## **What are your choices regarding cookies**

If you'd like to delete cookies or instruct your web browser to delete or refuse cookies, please visit the help pages of your web browser.

Please note, however, that if you delete cookies or refuse to accept them, you might not be able to use some or all of the features we offer. You may not be able to log in, store your preferences, and some of our pages might not display properly.

## **Cookies Tables**

The tables below list some of the internal and third-party cookies we use. As the names, numbers, and purposes of these cookies may change over time, this page may be updated to reflect those changes. For more information on how we use these cookies, please refer to ourCookies Policy above.

### **MoodleNet Cookies**


| **Cookie Name** | **Purpose**                        | **Expiration** |
| --------------- | ---------------------------------- | -------------- |
| locale          | Functionality (interface language) | n/a            |
| SESSION#ME      | Essential (user login session)     | n/a            |


### **Third-Party Cookies**

| **Provider Name** | **Cookie Name**   | **Purpose**                                             | **Expiration** | **More information**                                                  |
| ----------------- | ----------------- | ------------------------------------------------------- | -------------- | --------------------------------------------------------------------- |
| Algolia           | algolia-client-js | Analytical/performance & Tracking (search optimisation) | n/a            | More [details from Algolia](https://www.algolia.com/policies/cookies). |

  
  

